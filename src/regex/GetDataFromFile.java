/* 
 * The MIT License
 *
 * Copyright 2018 Marcin Cygan.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package regex;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import sql.TmpDataBase;

/**
 * Obiekt <code>GetDataFromFile</code> pobierający dane z pliku pdf
 *
 * @author Marcin Cygan
 * @version 1.0 10/04/2018
 */
public class GetDataFromFile {

    private final String line_str;
    private String[] split_str;

    private Boolean find_item;
    private String Citem;
    private final Map<String, String> item_data;
    private List<ItemData> ArrayItems;

    // dopasowania programu laser
    private final Pattern code;
    private final Pattern type_material;
    private final Pattern thickness;
    private final Pattern plan_dimension_x;
    private final Pattern plan_dimension_y;
    private final Pattern sheet_dimension_x;
    private final Pattern sheet_dimension_y;
    private final Pattern type_gas;
    private final Pattern cutting_time;
    private final Pattern weight_sheet;
    private final Pattern waste;
    private final Pattern item;
    // dopasowania pojedynczych detali
    private final Pattern code_item;
    private final Pattern name_item;
    // odpad
    private final List<LaserWaste> ArrayWaste;

    /**
     * Konstuktor domyślny inicjujący pola potrzebne do zaczytania danych z
     * pliku pdf
     *
     * @param line_str parametr do którego przekazywana jest linia tekstu
     */
    public GetDataFromFile(String line_str) {

        this.line_str = line_str;
        // dane dotyczące pojedynczych detali
        this.find_item = false;
        this.item_data = new HashMap<>();
        // dopasowania programu laser
        this.code = Pattern.compile("Nazwa planu cięcia: (.+) Plan,");
        this.type_material = Pattern.compile("Materiał: (.+)");
        this.thickness = Pattern.compile("Grubość blachy: (\\d{1,},\\d{1,}) mm");
        this.plan_dimension_x = Pattern.compile("Plan, wymiar X (\\d{1,},\\d{1,}) mm");
        this.plan_dimension_y = Pattern.compile("Plan, wymiar Y (\\d{1,},\\d{1,}) mm");
        this.sheet_dimension_x = Pattern.compile("Wymiar arkusza X (\\d{1,},\\d{1,}) mm");
        this.sheet_dimension_y = Pattern.compile("Wymiar arkusza Y (\\d{1,},\\d{1,}) mm");
        this.type_gas = Pattern.compile("Rodzaj gazu: (.+)");
        this.cutting_time = Pattern.compile("Czas cięcia (\\d{1,}:\\d{1,}:\\d{1,}) "
                + "Czasy dodatkowe (\\d{1,}:\\d{1,}:\\d{1,}) "
                + "Czas łączny (\\d{1,}:\\d{1,}:\\d{1,})");
        this.weight_sheet = Pattern.compile("Masa arkusza wyjściowego: (\\d{1,},\\d{1,})");
        this.waste = Pattern.compile("(.+) (\\d{1,},\\d{1,}) (\\d{1,},\\d{1,}) (\\d{1,},\\d{1,})");
        this.item = Pattern.compile("\\d{1,} (.+) (\\d{1,},\\d{1,}) mm (\\d{1,},\\d{1,}) "
                + "mm (\\d{1,},\\d{1,}) kg (\\d{1,}:\\d{1,}:\\d{1,}) s (\\d{1,})");
        // dopasowania pojedynczych detali
        this.code_item = Pattern.compile("Nazwa (.+) Zadane");
        this.name_item = Pattern.compile("Opis (.+) Wymiar");
        // lista detali
        this.ArrayItems = new ArrayList<>();
        // odpad
        this.ArrayWaste = new ArrayList<>();

    }

    /**
     * Prywatna metoda zamieniająca tekst w formacie <i>hh:mm:ss</i> na liczbę
     * sekund
     *
     * @param cutting_time_str tekst w formacie <i>hh:mm:ss</i>
     * @return zwraca liczbę całkowitą sekund
     */
    private Integer countTime(String cutting_time_str) {

        Integer cutting_time_i;
        Integer h, min, sec;

        h = Integer.parseInt(cutting_time_str.substring(0, 2));
        min = Integer.parseInt(cutting_time_str.substring(3, 5));
        sec = Integer.parseInt(cutting_time_str.substring(6, 8));
        cutting_time_i = (h * 3600) + (min * 60) + sec;

        return cutting_time_i;
    }

    /**
     * Prywatna metoda uzupełniająca pola obiektu <code>program</code>
     *
     * @param program obiekt <code>program</code>
     * @param line pojedyncza linia tekstu
     * @return zwraca true gdy metoda wykona się prawidłowo, kończąc działanie
     * metody
     */
    private boolean GetPattern(LaserProgram program, String line) {

        Matcher m_material, m_thickness, m_plan_dimension_x, m_plan_dimension_y,
                m_sheet_dimension_x, m_sheet_dimension_y, m_type_gas,
                m_cutting_time, m_weight_sheet, m_waste, m_item;

        m_material = this.type_material.matcher(line);
        m_thickness = this.thickness.matcher(line);
        m_plan_dimension_x = this.plan_dimension_x.matcher(line);
        m_plan_dimension_y = this.plan_dimension_y.matcher(line);
        m_sheet_dimension_x = this.sheet_dimension_x.matcher(line);
        m_sheet_dimension_y = this.sheet_dimension_y.matcher(line);
        m_type_gas = this.type_gas.matcher(line);
        m_cutting_time = this.cutting_time.matcher(line);
        m_weight_sheet = this.weight_sheet.matcher(line);
        m_waste = this.waste.matcher(line);
        m_item = this.item.matcher(line);

        if (m_material.find()) {

            program.setType_material(m_material.group(1));
            return true;

        } else if (m_thickness.find()) {

            program.setThickness(Double.parseDouble(m_thickness.group(1)
                    .replaceAll(",", ".")));
            return true;

        } else if (m_plan_dimension_x.find()) {

            program.setPlan_dimension_x(Double.parseDouble(m_plan_dimension_x.group(1)
                    .replaceAll(",", ".")));
            return true;

        } else if (m_plan_dimension_y.find()) {

            program.setPlan_dimension_y(Double.parseDouble(m_plan_dimension_y.group(1)
                    .replaceAll(",", ".")));
            return true;

        } else if (m_sheet_dimension_x.find()) {

            program.setSheet_dimension_x(Double.parseDouble(m_sheet_dimension_x.group(1)
                    .replaceAll(",", ".")));
            return true;

        } else if (m_sheet_dimension_y.find()) {

            program.setSheet_dimension_y(Double.parseDouble(m_sheet_dimension_y.group(1)
                    .replaceAll(",", ".")));
            return true;

        } else if (m_type_gas.find()) {

            program.setType_gas(m_type_gas.group(1));
            return true;

        } else if (m_cutting_time.find()) {

            String cutting_time_str;
            Integer cutting_time_i;
            // Czas cięcia
            cutting_time_str = m_cutting_time.group(1);
            cutting_time_i = countTime(cutting_time_str);
            program.setCutting_time(cutting_time_i);
            // Czasy dodatkowe
            cutting_time_str = m_cutting_time.group(2);
            cutting_time_i = countTime(cutting_time_str);
            program.setAdditional_time(cutting_time_i);
            // Czas łączny
            cutting_time_str = m_cutting_time.group(3);
            cutting_time_i = countTime(cutting_time_str);
            program.setTotal_time(cutting_time_i);
            return true;

        } else if (m_weight_sheet.find()) {

            program.setWeight_sheet(Double.parseDouble(m_weight_sheet.group(1)
                    .replaceAll(",", ".")));
            return true;

        } else if (m_waste.find()) {

            ArrayWaste.add(new LaserWaste());
            ArrayWaste.get(ArrayWaste.size() - 1).setName(m_waste.group(1));
            ArrayWaste.get(ArrayWaste.size() - 1).setPlan_dimension_x(Double.parseDouble(m_waste.group(2)
                    .replaceAll(",", ".")));
            ArrayWaste.get(ArrayWaste.size() - 1).setPlan_dimension_y(Double.parseDouble(m_waste.group(3)
                    .replaceAll(",", ".")));
            ArrayWaste.get(ArrayWaste.size() - 1).setMass(Double.parseDouble(m_waste.group(4)
                    .replaceAll(",", ".")));
            ArrayWaste.get(ArrayWaste.size() - 1).setLaser_program_code(program.getCode());

            return true;

        } else if (m_item.find()) {

            String name = this.item_data.get(m_item.group(1));
            program.setCodeItem(m_item.group(1));
            program.setNameItem(name);
            program.setDimension_xItem(Double.parseDouble(m_item.group(2)
                    .replaceAll(",", ".")));
            program.setDimension_yItem(Double.parseDouble(m_item.group(3)
                    .replaceAll(",", ".")));
            program.setMassItem(Double.parseDouble(m_item.group(4)
                    .replaceAll(",", ".")));

            String cutting_time_str;
            Integer cutting_time_i;
            // Czas cięcia
            cutting_time_str = m_item.group(5);
            cutting_time_i = countTime(cutting_time_str);
            program.setCutting_timeItem(cutting_time_i);
            program.setNumber_itemsItem(Integer.parseInt(m_item.group(6)));

        }

        return false;
    }

    /**
     * Metoda mająca na zadanie "wyłapać" nazwy wypalanych detali
     *
     * @param line pojedyncza linia tekstu
     * @return zwraca true gdy metoda wykona się prawidłowo, kończąc działanie
     * metody
     */
    private boolean GetPattern(String line) {

        Matcher matcher;

        matcher = this.code_item.matcher(line);
        if (matcher.find()) {
            this.Citem = matcher.group(1);
            this.find_item = true;
            return true;
        }

        if (this.find_item) {
            matcher = this.name_item.matcher(line);
            if (matcher.find()) {
                this.item_data.put(this.Citem, matcher.group(1));
            } else {
                this.item_data.put(this.Citem, "brak nazwy");
            }

            this.find_item = false;
            return true;
        }
        return false;
    }

    /**
     * Metoda uzupełniająca tymczasową bazę danych pobranych z pliku pdf
     */
    public void GetData() {

        split_str = this.line_str.split("\n");

        Matcher matcher;

        // zmienne programy laser
        List<LaserProgram> ArrayProgram = new ArrayList<>();
        Integer ia = -1;

        for (String line : split_str) {

            // uzupełnianie nazw detali - początek pliku
            GetPattern(line);

            // w przypadku gdy znajdziemy kod programu laser
            matcher = this.code.matcher(line);
            if (matcher.find()) {
                ia++;
                ArrayProgram.add(new LaserProgram());
                ArrayProgram.get(ia).setCode(matcher.group(1));
                ArrayProgram.get(ia).setName("PROGRAM LASER");
            }

            if (ia >= 0) {
                GetPattern(ArrayProgram.get(ia), line);
            }
        }

        // uzupełnianie tymczasowej bazy danch
        TmpDataBase sql_tmp = new TmpDataBase();
        int id_programy, id_detale, id_odpady;
        // programy na laser
        for (int i = 0; i < ArrayProgram.size(); i++) {
            LaserProgram program = ArrayProgram.get(i);
            sql_tmp.insertProgram(program.getCode(), program.getName(),
                    program.getType_material(), program.getThickness(),
                    program.getPlan_dimension_x(), program.getPlan_dimension_y(),
                    program.getSheet_dimension_x(), program.getSheet_dimension_y(),
                    program.getType_gas(), program.getCutting_time(),
                    program.getAdditional_time(), program.getTotal_time(),
                    program.getWeight_sheet());
            id_programy = sql_tmp.getProgramId(program.getCode());

            this.ArrayItems = program.getItemArray();
            for (int j = 0; j < this.ArrayItems.size(); j++) {

                sql_tmp.insertDetal(ArrayItems.get(j).getCode(),
                        ArrayItems.get(j).getName(), ArrayItems.get(j).getDimension_x(),
                        ArrayItems.get(j).getDimension_y(), ArrayItems.get(j).getMass(),
                        ArrayItems.get(j).getCutting_time());
                id_detale = sql_tmp.getItemId(ArrayItems.get(j).getCode());

                sql_tmp.insertDetaleNaProgramach(id_programy, id_detale,
                        ArrayItems.get(j).getNumber_items());
            }
        }

        // odpad
        for (int j = 0; j < ArrayWaste.size(); j++) {
            sql_tmp.insertOdpady(ArrayWaste.get(j).getName(),
                    ArrayWaste.get(j).getPlan_dimension_x(),
                    ArrayWaste.get(j).getPlan_dimension_y(),
                    ArrayWaste.get(j).getMass());

            id_programy = sql_tmp.getProgramId(ArrayWaste.get(j).getLaser_program_code());
            id_odpady = sql_tmp.getWasteId(ArrayWaste.get(j).getName());
            sql_tmp.insertOdpadyNaProgramach(id_odpady, id_programy);

        }

        sql_tmp.closeConnection();
    }
}
