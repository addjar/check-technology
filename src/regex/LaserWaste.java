/* 
 * The MIT License
 *
 * Copyright 2018 Marcin Cygan.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package regex;

/**
 * Obiekt <code>LaserWaste</code> odpadu na programie laser
 *
 * @author Marcin Cygan
 * @version 1.0 10/04/2018
 */
public class LaserWaste {

    private String name;
    private String laser_program_code;
    private Double plan_dimension_x;
    private Double plan_dimension_y;
    private Double mass;

    /**
     * Metoda nadająca nazwę odpadu
     *
     * @param name nazwa odpadu
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Metoda pobierająca nazwę odpadu
     *
     * @return nazwę odpadu
     */
    public String getName() {
        return this.name;
    }

    /**
     * Medoda dodająca kod programu na którym występuje odpad
     *
     * @param laser_program_code kod programu laser
     */
    public void setLaser_program_code(String laser_program_code) {
        this.laser_program_code = laser_program_code;
    }

    /**
     * Metoda pobierająca kod programu na którym występuja odpad
     *
     * @return kod programu laser
     */
    public String getLaser_program_code() {
        return this.laser_program_code;
    }

    /**
     * Metoda nadająca rozmiar x arkusz odpadowego
     *
     * @param plan_dimension_x rozmiar x arkusza odpadowego
     */
    public void setPlan_dimension_x(Double plan_dimension_x) {
        this.plan_dimension_x = plan_dimension_x;
    }

    /**
     * Metoda pobierająca rozmiar x arkusza odpadowego
     *
     * @return rozmiar x arkusza odpadowego
     */
    public Double getPlan_dimension_x() {
        return this.plan_dimension_x;
    }

    /**
     * Metoda nadająca rozmiar y arkusz odpadowego
     *
     * @param plan_dimension_y rozmiar y arkusza odpadowego
     */
    public void setPlan_dimension_y(Double plan_dimension_y) {
        this.plan_dimension_y = plan_dimension_y;
    }

    /**
     * Metoda pobierająca rozmiar y arkusza odpadowego
     *
     * @return rozmiar y arkusza odpadowego
     */
    public Double getPlan_dimension_y() {
        return this.plan_dimension_y;
    }

    /**
     * Metoda nadająca masę arkusza odpadowego
     *
     * @param mass masa arkusza odpadowego
     */
    public void setMass(Double mass) {
        this.mass = mass;
    }

    /**
     * Metoda pobierająca masę arkusza odpadowego
     *
     * @return masa arkusza odpadowego
     */
    public Double getMass() {
        return this.mass;
    }
}
