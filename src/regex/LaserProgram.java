/* 
 * The MIT License
 *
 * Copyright 2018 Marcin Cygan.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package regex;

import java.util.ArrayList;
import java.util.List;

/**
 * Obiekt <code>LaserProgram</code> programu na laser
 *
 * @author Marcin Cygan
 * @version 1.0 10/04/2018
 */
public class LaserProgram extends ItemData {

    private String code;
    private String name;
    private String type_material;
    private Double thickness;
    // obszar palenia
    private Double plan_dimension_x;
    private Double plan_dimension_y;
    // rozmiar arkusza
    private Double sheet_dimension_x;
    private Double sheet_dimension_y;
    private String type_gas;
    // czas mierzony w sekundach
    private Integer cutting_time;
    private Integer additional_time;
    private Integer total_time;
    // masa arkusza z odpadem
    private Double weight_sheet;

    // detale
    private final List<ItemData> ArrayItems;

    /**
     * Konstruktor domyślny inicjujący pola obiektu <code>LaserProgram</code>
     */
    public LaserProgram() {

        ArrayItems = new ArrayList<>();
    }

    /**
     * Metoda nadająca kod programu
     *
     * @param code kod programu
     */
    @Override
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Metoda pobierająca kod programu
     *
     * @return nazwę programu
     */
    @Override
    public String getCode() {
        return this.code;
    }

    /**
     * Metoda nadająca nazwę programu
     *
     * @param name nazwa programu
     */
    @Override
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Metoda pobierająca nazę programu
     *
     * @return nazwę programu
     */
    @Override
    public String getName() {
        return this.name;
    }

    /**
     * Metoda nadająca typ materiału (blach)
     *
     * @param type_material typ materiału
     */
    public void setType_material(String type_material) {
        this.type_material = type_material;
    }

    /**
     * Metoda pobierająca typ materiału (blachy)
     *
     * @return typ materiału
     */
    public String getType_material() {
        return this.type_material;
    }

    /**
     * Metoda nadająca grubość blachy
     *
     * @param thickness grubość blachy
     */
    public void setThickness(Double thickness) {
        this.thickness = thickness;
    }

    /**
     * Metoda pobierająca grubość blachy
     *
     * @return grubość blachy
     */
    public Double getThickness() {
        return this.thickness;
    }

    /**
     * Metoda nadająca wymiar planu palenia x
     *
     * @param plan_dimension_x wymiar planu palenia x
     */
    public void setPlan_dimension_x(Double plan_dimension_x) {
        this.plan_dimension_x = plan_dimension_x;
    }

    /**
     * Metoda pobierająca wymiar planu palenia x
     *
     * @return wymiar planu palenia x
     */
    public Double getPlan_dimension_x() {
        return this.plan_dimension_x;
    }

    /**
     * Metoda nadająca wymiar planu palenia y
     *
     * @param plan_dimension_y plan palenia y
     */
    public void setPlan_dimension_y(Double plan_dimension_y) {
        this.plan_dimension_y = plan_dimension_y;
    }

    /**
     * Metoda pobierająca wymiar planu palenia y
     *
     * @return plan palenia y
     */
    public Double getPlan_dimension_y() {
        return this.plan_dimension_y;
    }

    /**
     * Metada nadająca wymiar arkusza blachy x
     *
     * @param sheet_dimension_x wymiar arkusza blachy x
     */
    public void setSheet_dimension_x(Double sheet_dimension_x) {
        this.sheet_dimension_x = sheet_dimension_x;
    }

    /**
     * Metoda pobierająca wymiar arkusza blachy x
     *
     * @return wymiar arkudza blachy x
     */
    public Double getSheet_dimension_x() {
        return this.sheet_dimension_x;
    }

    /**
     * Metoda nadająca wymiar arkusza blachy y
     *
     * @param sheet_dimension_y wymiar arkusza blachy y
     */
    public void setSheet_dimension_y(Double sheet_dimension_y) {
        this.sheet_dimension_y = sheet_dimension_y;
    }

    /**
     * Metoda pobierająca wymiar arkusza blachy y
     *
     * @return wymiar arkusza balcht y
     */
    public Double getSheet_dimension_y() {
        return this.sheet_dimension_y;
    }

    /**
     * Metoda nadająca typ gazu użytago do palenia
     *
     * @param type_gas typ gazu użytego do palenia
     */
    public void setType_gas(String type_gas) {
        this.type_gas = type_gas;
    }

    /**
     * Metoda pobierająca typ gazu użytego do palenia
     *
     * @return typ gazu użytego do palenia
     */
    public String getType_gas() {
        return this.type_gas;
    }

    /**
     * Metoda nadająca czas palenia programu
     *
     * @param cutting_time czas palenia programu
     */
    @Override
    public void setCutting_time(Integer cutting_time) {
        this.cutting_time = cutting_time;
    }

    /**
     * Metoda pobierająca czas palenia programu
     *
     * @return czas palenia programu
     */
    @Override
    public Integer getCutting_time() {
        return this.cutting_time;
    }

    /**
     * Metoda nadająca czas dodatkowy palenia programu
     *
     * @param additional_time czas dodatkowy palenia programu
     */
    public void setAdditional_time(Integer additional_time) {
        this.additional_time = additional_time;
    }

    /**
     * Metoda pobierająca czas dodatkowy palenia programu
     *
     * @return czas dodatkowy palenia programu
     */
    public Integer getAdditional_time() {
        return this.additional_time;
    }

    /**
     * Metoda nadająca czas łączny palenia programu
     *
     * @param total_time czas łaczny palenia programu
     */
    public void setTotal_time(Integer total_time) {
        this.total_time = total_time;
    }

    /**
     * Metoda pobierająca czas łączny palenia programu
     *
     * @return czas łaczny palenia programu
     */
    public Integer getTotal_time() {
        return this.total_time;
    }

    /**
     * Metoda nadająca masę arkusza wejściowego
     *
     * @param weight_sheet masa arkusza wejściowego
     */
    public void setWeight_sheet(Double weight_sheet) {
        this.weight_sheet = weight_sheet;
    }

    /**
     * Metoda pobierająca masę arkusza wejściowego
     *
     * @return masę arkusza wejściowego
     */
    public Double getWeight_sheet() {
        return this.weight_sheet;
    }

    /**
     * Metoda dodająca listę kodów detali występujących na programie
     *
     * @param code kod detalu
     */
    public void setCodeItem(String code) {
        ArrayItems.add(new ItemData());
        ArrayItems.get(ArrayItems.size() - 1).setCode(code);
    }

    /**
     * Metoda dodająca listę nazw detali występujących na programie
     *
     * @param name nazwa detalu
     */
    public void setNameItem(String name) {
        ArrayItems.get(ArrayItems.size() - 1).setName(name);
    }

    /**
     * Metoda dodająca wymiar x detali występujących na programie
     *
     * @param dimension_x wymiar x detalu
     */
    public void setDimension_xItem(Double dimension_x) {
        ArrayItems.get(ArrayItems.size() - 1).setDimension_x(dimension_x);
    }

    /**
     * Metoda dodająca wymiar y detali występujących na programie
     *
     * @param dimension_y wymiar y detalu
     */
    public void setDimension_yItem(Double dimension_y) {
        ArrayItems.get(ArrayItems.size() - 1).setDimension_y(dimension_y);
    }

    /**
     * Metoda dodająca masę detali występujących na programie
     *
     * @param mass masa detalu
     */
    public void setMassItem(Double mass) {
        ArrayItems.get(ArrayItems.size() - 1).setMass(mass);
    }

    /**
     * Metoda dodająca czas cięcia detali występujących na programie
     *
     * @param cutting_time czas cięcia detalu
     */
    public void setCutting_timeItem(Integer cutting_time) {
        ArrayItems.get(ArrayItems.size() - 1).setCutting_time(cutting_time);
    }

    /**
     * Metoda dodająca ilość detali występujących na programie (dotyczy
     * konkretnej sztuki)
     *
     * @param number_items ilość detalu
     */
    public void setNumber_itemsItem(Integer number_items) {
        ArrayItems.get(ArrayItems.size() - 1).setNumber_items(number_items);
    }

    /**
     * Metoda pobierająca listę detali występujących na programie
     *
     * @return listę detali występujących na programie
     */
    public List<ItemData> getItemArray() {
        return this.ArrayItems;
    }

    /**
     * Metoda zwraca wielkość listy z detalami
     *
     * @return wilkość listy z detalami
     */
    public Integer getItemArraySize() {
        return this.ArrayItems.size();
    }
}
