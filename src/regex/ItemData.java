/* 
 * The MIT License
 *
 * Copyright 2018 Marcin Cygan.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package regex;

/**
 * Obiekt <code>ItemData</code> pojedynczego wypalanego detalu
 *
 * @author Marcin Cygan
 * @version 1.0 10/04/2018
 */
public class ItemData {

    private String code;
    private String name;
    private Double dimension_x;
    private Double dimension_y;
    private Double mass;
    // czas mierzony w sekundach
    private Integer cutting_time;
    private Integer number_items;

    /**
     * Metoda nadająca kod detalowi
     *
     * @param code kod detalu
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Metoda pobierająca kod detalu
     *
     * @return kod detalu
     */
    public String getCode() {
        return this.code;
    }

    /**
     * Metoda nadająca nazwę detalowi
     *
     * @param name nazwa detalu
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Metoda pobierająca nazwę detalu
     *
     * @return nazwę detalu
     */
    public String getName() {
        return this.name;
    }

    /**
     * Metoda nadająca współrzędną x detalu
     *
     * @param dimension_x współrzędna x detalu
     */
    public void setDimension_x(Double dimension_x) {
        this.dimension_x = dimension_x;
    }

    /**
     * Metoda pobierająca współrzędną x detalu
     *
     * @return wsþółrzędną x detalu
     */
    public Double getDimension_x() {
        return this.dimension_x;
    }

    /**
     * Metoda nadająca współrzędną y detalu
     *
     * @param dimension_y współrzędna y detalu
     */
    public void setDimension_y(Double dimension_y) {
        this.dimension_y = dimension_y;
    }

    /**
     * Metoda pobierająca współrzędną y detalu
     *
     * @return wsþółrzędną y detalu
     */
    public Double getDimension_y() {
        return this.dimension_y;
    }

    /**
     * Metoda nadająca masę detalowi
     *
     * @param mass masa detalu
     */
    public void setMass(Double mass) {
        this.mass = mass;
    }

    /**
     * Metoda pobierająca masę detalu
     *
     * @return masę detalu
     */
    public Double getMass() {
        return this.mass;
    }

    /**
     * Metoda uzupełniająca czas cięcia detalu
     *
     * @param cutting_time czas cięcia detalu
     */
    public void setCutting_time(Integer cutting_time) {
        this.cutting_time = cutting_time;
    }

    /**
     * Metoda pobierająca czas cięcia detalu
     *
     * @return czas cięcia detalu
     */
    public Integer getCutting_time() {
        return this.cutting_time;
    }

    /**
     * Metoda uzupełniająca ilość detali
     *
     * @param number_items ilość detali
     */
    public void setNumber_items(Integer number_items) {
        this.number_items = number_items;
    }

    /**
     * Metoda pobierająca ilość detali
     *
     * @return ilość detali
     */
    public Integer getNumber_items() {
        return this.number_items;
    }

}
