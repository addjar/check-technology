/* 
 * The MIT License
 *
 * Copyright 2018 Marcin Cygan.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Obiekt <code>TmpDataBase</code> obsługujący tymczasową bazę danych
 *
 * @author Marcin Cygan
 * @version 1.0 10/04/2018
 */
public class TmpDataBase {

    public static final String DRIVER = "org.sqlite.JDBC";
    public static final String DB_URL = "jdbc:sqlite:./SQL/tmp.db";

    private Connection conn;
    private Statement stat;

    /**
     * Konstruktor domyślny inicjujący tymczasową bazę danych
     */
    public TmpDataBase() {

        try {
            Class.forName(TmpDataBase.DRIVER);
        } catch (ClassNotFoundException e) {
            System.err.println("Brak sterownika JDBC");
        }

        try {
            conn = DriverManager.getConnection(DB_URL);
            stat = conn.createStatement();
        } catch (SQLException e) {
            System.err.println("Problem z otwarciem polaczenia");
        }
    }

    /**
     * Metoda tworząca podstawową strukturę danych, tymczasowej bazy danych
     *
     * @return zwraca true gdy metoda wykona się prawidłowo, false w przeciwnym
     * wypadku
     */
    public boolean createTables() {

        String createDetale = "CREATE TABLE IF NOT EXISTS detale "
                + "(id_detale INTEGER PRIMARY KEY AUTOINCREMENT, kod VARCHAR(255), "
                + "nazwa VARCHAR(255), wymiar_x REAL, wymiar_y REAL, masa REAL, "
                + "czas_ciecia INTEGER)";

        String createDetaleNaProgramach = "CREATE TABLE IF NOT EXISTS detale_na_programach "
                + "(id INTEGER PRIMARY KEY AUTOINCREMENT, id_programy INTEGER, "
                + "id_detale INTEGER, ilosc INTEGER)";

        String createOdpadyNaProgramach = "CREATE TABLE IF NOT EXISTS "
                + "odpady_na_programach (id INTEGER PRIMARY KEY AUTOINCREMENT, id_odpady INTEGER, id_programy INTEGER)";

        String createProgramy = "CREATE TABLE IF NOT EXISTS programy(id_programy "
                + "INTEGER PRIMARY KEY AUTOINCREMENT, kod VARCHAR(255), nazwa VARCHAR(255), "
                + "material VARCHAR(255), grubosc  REAL, plan_wymiar_x REAL, "
                + "plan_wymiar_y REAL, wymiar_arkusza_x REAL, wymiar_arkusza_y "
                + "REAL, rodzaj_gazu  VARCHAR(255), czas_ciecia INTEGER, "
                + "czasy_dodatkowe INTEGER, czas_laczny INTEGER, "
                + "masa_arkusz_wejsciowego REAL)";

        String createOdpady = "CREATE TABLE IF NOT EXISTS odpady(id_odpady "
                + "INTEGER PRIMARY KEY AUTOINCREMENT, nazwa VARCHAR(255), wymiar_odpadu_x REAL,"
                + " wymiar_odpadu_y REAL, masa_odpadu REAL)";

        try {
            stat.execute(createDetale);
            stat.execute(createProgramy);
            stat.execute(createOdpady);
            stat.execute(createDetaleNaProgramach);
            stat.execute(createOdpadyNaProgramach);

        } catch (SQLException e) {
            System.err.println("Blad przy tworzeniu tabeli");
            return false;
        }
        return true;
    }

    /**
     * Metoda uzupełniająca tabelę danymi z wypalanych detali
     *
     * @param kod kod wypalanego detalu
     * @param nazwa nazwa wypalanego detalu
     * @param wymiar_x wymiar x wypalanego detalu
     * @param wymiar_y wymiar y wypalanego detalu
     * @param masa masa wypalanego detalu
     * @param czas_ciecia czas cięcia wypalanego detalu
     * @return zwraca true gdy metoda wykona się prawidłowo, false w przeciwnym
     * wypadku
     */
    public boolean insertDetal(String kod, String nazwa, Double wymiar_x,
            Double wymiar_y, Double masa, Integer czas_ciecia) {

        try {
            PreparedStatement prepStmt = conn.prepareStatement(
                    "insert into detale values (NULL, ?, ?, ?, ?, ?, ?);");
            prepStmt.setString(1, kod);
            prepStmt.setString(2, nazwa);
            prepStmt.setDouble(3, wymiar_x);
            prepStmt.setDouble(4, wymiar_y);
            prepStmt.setDouble(5, masa);
            prepStmt.setInt(6, czas_ciecia);
            prepStmt.execute();
        } catch (SQLException e) {
            System.err.println("Blad przy uzupełnianiu tabeli z detalami");
            return false;
        }

        return true;
    }

    /**
     * Metoda uzupełniająca tabelę danymi na temat użytych programów na laser
     *
     * @param kod kod programu na laser
     * @param nazwa nazwa programu na laser
     * @param material materiał - gatunek użytej stali
     * @param grubosc grubość blachy
     * @param plan_wymiar_x wymiar x faktycznego planu palenia
     * @param plan_wymiar_y wymiar y faktycznego planu palenia
     * @param wymiar_arkusza_x wymiar x arkusza blachy
     * @param wymiar_arkusza_y wymiar y arkusza blachy
     * @param rodzaj_gazu rodzaj użytego gazu
     * @param czas_ciecia czas samego wypalania laserowego
     * @param czasy_dodatkowe czasy dodatkowe wypalania laserowego
     * @param czas_laczny czas łączny wypalania laserowego
     * @param masa_arkusz_wejsciowego masa arkusza blachy
     * @return zwraca true gdy metoda wykona się prawidłowo, false w przeciwnym
     * wypadku
     */
    public boolean insertProgram(String kod, String nazwa, String material,
            Double grubosc, Double plan_wymiar_x, Double plan_wymiar_y,
            Double wymiar_arkusza_x, Double wymiar_arkusza_y, String rodzaj_gazu,
            Integer czas_ciecia, Integer czasy_dodatkowe, Integer czas_laczny,
            Double masa_arkusz_wejsciowego) {

        try {
            PreparedStatement prepStmt = conn.prepareStatement(
                    "insert into programy values (NULL, ?, ?, ?, ?, ?, ?, ?, ?, "
                    + "?, ?, ?, ?, ?);");
            prepStmt.setString(1, kod);
            prepStmt.setString(2, nazwa);
            prepStmt.setString(3, material);
            prepStmt.setDouble(4, grubosc);
            prepStmt.setDouble(5, plan_wymiar_x);
            prepStmt.setDouble(6, plan_wymiar_y);
            prepStmt.setDouble(7, wymiar_arkusza_x);
            prepStmt.setDouble(8, wymiar_arkusza_y);
            prepStmt.setString(9, rodzaj_gazu);
            prepStmt.setInt(10, czas_ciecia);
            prepStmt.setInt(11, czasy_dodatkowe);
            prepStmt.setInt(12, czas_laczny);
            prepStmt.setDouble(13, masa_arkusz_wejsciowego);
            prepStmt.execute();
        } catch (SQLException e) {
            System.err.println("Blad przy uzupełnianiu tabeli z programem");
            return false;
        }

        return true;
    }

    /**
     * Metoda uzupełniająca tabelę danymi na temat odpadów, które później mogą
     * być jeszcze użyte
     *
     * @param nazwa nazwa odpadu
     * @param wymiar_odpadu_x wymiar x odpadu
     * @param wymiar_odpadu_y wymiar y odpadu
     * @param masa_odpadu masa odpadu
     * @return zwraca true gdy metoda wykona się prawidłowo, false w przeciwnym
     * wypadku
     */
    public boolean insertOdpady(String nazwa, Double wymiar_odpadu_x,
            Double wymiar_odpadu_y, Double masa_odpadu) {

        try {
            PreparedStatement prepStmt = conn.prepareStatement(
                    "insert into odpady values (NULL, ?, ?, ?, ?);");
            prepStmt.setString(1, nazwa);
            prepStmt.setDouble(2, wymiar_odpadu_x);
            prepStmt.setDouble(3, wymiar_odpadu_y);
            prepStmt.setDouble(4, masa_odpadu);
            prepStmt.execute();
        } catch (SQLException e) {
            System.err.println("Blad przy uzupełnianiu tabeli z odpadami");
            return false;
        }

        return true;
    }

    /**
     * Metoda uzupełniająca tabelę danymi na temat występujących detali na
     * programach jest to powiązanie logiczne
     *
     * @param id_programy unikatowy numer programów na laser
     * @param id_detale unikatowy numer detali na programach
     * @param ilosc ilość detalu występująca na danym programie
     * @return zwraca true gdy metoda wykona się prawidłowo, false w przeciwnym
     * wypadku
     */
    public boolean insertDetaleNaProgramach(Integer id_programy, Integer id_detale,
            Integer ilosc) {

        try {
            PreparedStatement prepStmt = conn.prepareStatement(
                    "insert into detale_na_programach values (NULL, ?, ?, ?);");
            prepStmt.setInt(1, id_programy);
            prepStmt.setInt(2, id_detale);
            prepStmt.setInt(3, ilosc);
            prepStmt.execute();
        } catch (SQLException e) {
            System.err.println("Blad przy uzupełnianiu tabeli detale na programach");
            return false;
        }
        return true;
    }

    /**
     * Metoda uzupełniająca tabelę danymi na temat odpadów na programach jest to
     * powiązanie logiczne
     *
     * @param id_odpady unikatowy numer odpadów
     * @param id_programy unikatowy numer programów na laser
     * @return zwraca true gdy metoda wykona się prawidłowo, false w przeciwnym
     * wypadku
     */
    public boolean insertOdpadyNaProgramach(Integer id_odpady, Integer id_programy) {

        try {
            PreparedStatement prepStmt = conn.prepareStatement(
                    "insert into odpady_na_programach values (NULL, ?, ?);");
            prepStmt.setInt(1, id_odpady);
            prepStmt.setInt(2, id_programy);
            prepStmt.execute();
        } catch (SQLException e) {
            System.err.println("Blad przy uzupełnianiu tabeli odpady na programach");
            return false;
        }
        return true;
    }

    /**
     * Metoda zamykająca połączenie z tymczasową bazą danch
     */
    public void closeConnection() {
        try {
            conn.close();
        } catch (SQLException e) {
            System.err.println("Problem z zamknieciem polaczenia");
        }
    }

    /**
     * Metoda pobierająca unikatowy numer programu na laser
     *
     * @param kod kod programu na laser
     * @return zwraca unikatowy numer programu, null w przeciwnym wypadku
     */
    public Integer getProgramId(String kod) {

        int id;
        try {

            ResultSet result = stat.executeQuery(String.format("SELECT id_programy FROM programy WHERE kod=\'%s\'", kod));
            id = result.getInt("id_programy");

        } catch (SQLException e) {
            return null;
        }

        return id;
    }

    /**
     * Metoda pobierająca unikatowy numer wypalanego detalu
     *
     * @param kod kod wypalanego detalu
     * @return zwraca unikatowy numer detalu, null w przeciwnym wypadku
     */
    public Integer getItemId(String kod) {

        int id;
        try {

            ResultSet result = stat.executeQuery(String.format("SELECT id_detale FROM detale WHERE kod=\'%s\'", kod));
            id = result.getInt("id_detale");

        } catch (SQLException e) {
            return null;
        }
        return id;
    }

    /**
     * Metoda pobierająca unikatowy numer odpadu
     *
     * @param nazwa nazwa odpadu
     * @return zwraca unikatowy numer odpadu, null w przeciwnym wypadku
     */
    public Integer getWasteId(String nazwa) {

        int id;
        try {
            ResultSet result = stat.executeQuery(String.format("SELECT id_odpady FROM odpady WHERE nazwa=\'%s\'", nazwa));
            id = result.getInt("id_odpady");
        } catch (SQLException e) {
            return null;
        }
        return id;
    }

}
