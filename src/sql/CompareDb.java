/* 
 * The MIT License
 *
 * Copyright 2018 Marcin Cygan.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Obiekt <code>CompareDb</code> porównujący dwie bazy danych
 *
 * @author Marcin Cygan
 * @version 1.0 10/04/2018
 */
public class CompareDb {

    public static final String DRIVER = "org.sqlite.JDBC";
    public static final String DB_URL_TMP = "jdbc:sqlite:./SQL/tmp.db";
    public static final String DB_URL_PROD = "jdbc:sqlite:./SQL_BAZA_PROD/produkcja.db";

    private Connection conn_tmp;
    private Connection conn_prod;
    private Statement stat_tmp;
    private Statement stat_prod;

    /**
     * Konstruktor domyślny inicjujący połączenie do porównywanych baz danych
     */
    public CompareDb() {

        try {
            Class.forName(TmpDataBase.DRIVER);
        } catch (ClassNotFoundException e) {
            System.err.println("Brak sterownika JDBC");
        }

        try {
            conn_tmp = DriverManager.getConnection(DB_URL_TMP);
            conn_prod = DriverManager.getConnection(DB_URL_PROD);
            stat_tmp = conn_tmp.createStatement();
            stat_prod = conn_prod.createStatement();

        } catch (SQLException e) {
            System.err.println("Problem z otwarciem polaczenia");
        }
    }

    /**
     * Metoda pobierająca dane z bazy danych
     *
     * @param base parametr określający z której bazy danych będą pobierane dane
     * @param sql_question zapytanie SQL
     * @param column użyta kolumna do odczytania danych
     * @param data lista pobranych danych
     * @return zwraca dane w formie listy string(ów), w przypadku niepowodzenia
     * zwracany jest null
     */
    public List<String> getData(String base, String sql_question, String column, List<String> data) {

        ResultSet result;
        try {
            if ("tmp".equals(base)) {
                result = stat_tmp.executeQuery(sql_question);
            } else {
                result = stat_prod.executeQuery(sql_question);
            }

            while (result.next()) {
                data.add(result.getString(column));
            }
            return data;
        } catch (SQLException e) {
            System.err.println("Błąd pobierania danych! List<String> ");
            return null;
        }
    }

    /**
     * Przeciążona metoda pobierająca dane z bazy danych
     *
     * @param base parametr określający z której bazy danych będą pobierane dane
     * @param sql_question zapytanie SQL
     * @param column użyta kolumna do odczytania danych
     * @return zwraca dane w formie listy liczb całkowitych, w przypadku
     * niepowodzenia zwracany jest null
     */
    public List<Integer> getData(String base, String sql_question, String column) {

        List<Integer> data = new ArrayList<>();

        ResultSet result;
        try {
            if ("tmp".equals(base)) {
                result = stat_tmp.executeQuery(sql_question);
            } else {
                result = stat_prod.executeQuery(sql_question);
            }

            while (result.next()) {
                data.add(result.getInt(column));
            }
            return data;
        } catch (SQLException e) {
            System.err.println("Błąd pobierania danych!");
            return null;
        }
    }

    public void closeConnection() {
        try {
            conn_tmp.close();
            conn_prod.close();
        } catch (SQLException e) {
            System.err.println("Problem z zamknieciem polaczenia");
        }
    }

}
