/* 
 * The MIT License
 *
 * Copyright 2018 Marcin Cygan.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package pdf;

import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;
import java.io.IOException;

/**
 * Obiekt <code>PDFReader</code> parsujący pliki pdf
 *
 * @author Marcin Cygan
 * @version 1.0 10/04/2018
 */
public class PDFReader {

    String pdf_url;

    /**
     * Konstruktor domyślny inicjujący pola obiektu <code>PDFReader</code>
     *
     * @param pdf_url ścieżka do pliku pdf
     */
    public PDFReader(String pdf_url) {
        this.pdf_url = pdf_url;
    }

    /**
     * Metoda parsująca plik pdf
     *
     * @return zwraca zaczytany tekst
     */
    public String ReadPDF() {
        StringBuilder str = new StringBuilder();

        try {
            PdfReader reader = new PdfReader(this.pdf_url);
            int n = reader.getNumberOfPages();
            for (int i = 1; i < n + 1; i++) {
                String str2 = PdfTextExtractor.getTextFromPage(reader, i);
                str.append(str2);
            }

        } catch (IOException err) {

        }

        return String.format("%s", str);
    }

}
