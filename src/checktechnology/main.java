/* 
 * The MIT License
 *
 * Copyright 2018 Marcin Cygan.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package checktechnology;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.io.FileUtils;
import pdf.PDFReader;
import regex.GetDataFromFile;
import sql.CompareDb;
import sql.TmpDataBase;

/**
 *
 * @author Marcin Cygan
 * @version 1.0 10/04/2018
 */
public class main {

    public static void main(String[] args) throws IOException {

        //      jeżeli istnieje tymczasowa baza danych to ją usuwam
        String directoryPath = "./SQL";
        File file = new File(directoryPath);
        FileUtils.cleanDirectory(file);

        // tworzenie nowej bazy danch do przechowywania tymczasowych danych
        TmpDataBase sql_tmp = new TmpDataBase();
        sql_tmp.createTables();
        sql_tmp.closeConnection();

        // parsowanie dokumentów pdf wyeksportowanch z oprogramowania typu CAD/CAM
        // wypalarki laserowej
        File file_pdf = new File("./PDF/");
        String[] names = file_pdf.list();
        for (String name : names) {

            PDFReader pdf = new PDFReader(String.format("./PDF/%s", name));
            String str = pdf.ReadPDF();

            GetDataFromFile regex = new GetDataFromFile(str);
            regex.GetData();
        }

        // porównywanie baz danych, bazy produkcyjnej obsługiwanej poprzez oprogramowanie typu ERP
        // z bazą tymczasową utworzoną na podstawie plików pdf
        CompareDb sql = new CompareDb();
        List<String> tmp_program_list, prod_program_list, tmp_item_list, prod_item_list;
        List<Integer> tmp_item_value, prod_item_value;
        String base;
        String sql_question = "SELECT kod FROM programy";
        String column = "kod";

        tmp_program_list = sql.getData(base = "tmp", sql_question, column, new ArrayList<>());
        prod_program_list = sql.getData(base = "prod", sql_question, column, new ArrayList<>());

        for (String code : tmp_program_list) {

            if (prod_program_list.contains(code)) {
                System.out.println("Sprawdzamy program: " + code);
                // pobieranie listy kodów detali
                sql_question = String.format("SELECT detale.kod FROM detale INNER JOIN "
                        + "detale_na_programach ON detale.id_detale = detale_na_programach.id_detale "
                        + "INNER JOIN programy ON detale_na_programach.id_programy = programy.id_programy "
                        + "WHERE programy.kod = '%s'", code);

                tmp_item_list = sql.getData(base = "tmp", sql_question, column, new ArrayList<>());
                prod_item_list = sql.getData(base = "prod", sql_question, column, new ArrayList<>());

                for (String code_item : tmp_item_list) {

                    if (prod_item_list.contains(code_item)) {
                        // sprawdzanie ilości detalu

                        sql_question = String.format("SELECT detale_na_programach.ilosc "
                                + "FROM detale_na_programach INNER JOIN detale "
                                + "ON detale.id_detale = detale_na_programach.id_detale "
                                + "INNER JOIN programy ON detale_na_programach.id_programy = "
                                + "programy.id_programy WHERE programy.kod = '%s' "
                                + "AND detale.kod = '%s'", code, code_item);

                        tmp_item_value = sql.getData(base = "tmp", sql_question, "ilosc");
                        prod_item_value = sql.getData(base = "prod", sql_question, "ilosc");

                        int test;
                        test = tmp_item_value.get(0) - prod_item_value.get(0);
                        if (test > 0) {
                            System.out.println(String.format("Detali o kodzie: %s zwiększono o %d szt.", code_item, test));
                        } else if (test < 0) {
                            System.out.println(String.format("Detali o kodzie: %s zmniejszono o %d szt.", code_item, test * -1));
                        }

                    } else {
                        System.out.println(String.format("Kod: %s został dodany", code_item));
                    }
                }

                for (String code_item : prod_item_list) {

                    if (!tmp_item_list.contains(code_item)) {
                        System.out.println(String.format("Kod: %s został usunięty", code_item));
                    }
                }
                System.out.println("");
            } else {
                System.out.println(String.format("Brak programu: %s w bazie danch", code));
//                TODO: dodać metodę dodawania programu do bazy produkcyjnej
            }

        }

        sql.closeConnection();
    }

}
